﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class InstitutionType
    {
        public InstitutionType()
        {
            Institutions = new List<Institution>();
        }
        
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Institution> Institutions { get; set; }
    }
}
