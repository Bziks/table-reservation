﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class MenuInstitution
    {
        public int Id { get; set; }
        public int InstitutionId { get; set; }
        public int MenuCategoryId { get; set; }
        public int MenuRecipeId { get; set; }
        public int Position { get; set; }

        public Institution Institution { get; set; }
        public MenuCategory MenuCategory { get; set; }
        public MenuRecipe MenuRecipe { get; set; }
    }
}
