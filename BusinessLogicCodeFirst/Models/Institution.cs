﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class Institution
    {
        public Institution()
        {
            Users = new List<User>();
            ServiceTags = new List<ServiceTag>();
            MenuInstitutions = new List<MenuInstitution>();
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string CountryAbbr { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Logo { get; set; }
        [Column(TypeName = "ntext")]
        public string Description { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int InstitutionTypeId { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }

        public InstitutionType InstitutionType { get; set; }
        public virtual ICollection<User> Users { get; private set; }
        public virtual ICollection<ServiceTag> ServiceTags { get; set; }
        public virtual ICollection<Kitchen> Kitchens { get; set; }
        public virtual ICollection<MenuInstitution> MenuInstitutions { get; set; }

    }
}
