﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;

using Microsoft.AspNet.Identity.EntityFramework;

namespace BusinessLogic.Models
{
    public class DataContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>, IDataContext, IDisposable
    {
        public DataContext() : base("DataContext") { }

        public IDbSet<MenuCategory> MenuCategories { get; set; }
        public IDbSet<MenuInstitution> MenuInstitutions { get; set; }
        public IDbSet<MenuRecipe> MenuRecipes { get; set; }
        public IDbSet<ServiceTag> ServiceTags { get; set; }
        public IDbSet<Kitchen> Kitchens { get; set; }
        public IDbSet<WeightType> WeightTypes { get; set; }
        public IDbSet<InstitutionType> InstitutionTypes { get; set; }
        public IDbSet<Institution> Institutions { get; set; }
        public IDbSet<UserRole> UserRoles { get; set; }
        public IDbSet<UserLogin> UserLogins { get; set; }
        public IDbSet<UserClaim> UserClaims { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //Change Asp.Net Identity default table names
            modelBuilder.Entity<MenuCategory>().ToTable("MenuCategory");
            modelBuilder.Entity<MenuInstitution>().ToTable("MenuInstitution");
            modelBuilder.Entity<MenuRecipe>().ToTable("MenuRecipe");
            modelBuilder.Entity<ServiceTag>().ToTable("ServiceTag");
            modelBuilder.Entity<Kitchen>().ToTable("Kitchen");
            modelBuilder.Entity<WeightType>().ToTable("WeightType");

            modelBuilder.Entity<InstitutionType>().ToTable("InstitutionType");
            modelBuilder.Entity<Institution>().ToTable("Institution");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<UserRole>().ToTable("UserRole");
            modelBuilder.Entity<UserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<UserClaim>().ToTable("UserClaim");
        }
    }

    public interface IDataContext
    {
        IDbSet<MenuCategory> MenuCategories { get; set; }
        IDbSet<MenuInstitution> MenuInstitutions { get; set; }
        IDbSet<MenuRecipe> MenuRecipes { get; set; }
        IDbSet<ServiceTag> ServiceTags { get; set; }
        IDbSet<Kitchen> Kitchens { get; set; }
        IDbSet<WeightType> WeightTypes { get; set; }
        IDbSet<InstitutionType> InstitutionTypes { get; set; }
        IDbSet<Institution> Institutions { get; set; }
        IDbSet<User> Users { get; set; }
        IDbSet<Role> Roles { get; set; }
        IDbSet<UserRole> UserRoles { get; set; }
        IDbSet<UserLogin> UserLogins { get; set; }
        IDbSet<UserClaim> UserClaims { get; set; }

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}