﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class MenuCategory
    {
        public MenuCategory()
        {
            MenuInstitutions = new List<MenuInstitution>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }

        public User User { get; set; }
        public virtual ICollection<MenuInstitution> MenuInstitutions { get; set; }
    }
}
