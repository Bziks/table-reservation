﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class WeightType
    {
        public WeightType()
        {
            MenuRecipes = new List<MenuRecipe>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<MenuRecipe> MenuRecipes { get; set; }
    }
}
