﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class RoleNames
    {
        public const string User = "User";
        public const string Admin = "Admin";
        public const string Owner = "Owner";
        public const string Staff = "Staff";
    }
}
