﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Models
{
    public class MenuRecipe
    {
        public MenuRecipe()
        {
            MenuInstitutions = new List<MenuInstitution>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        [Column(TypeName = "text")]
        public string Description { get; set; }
        public int WeightTypeId { get; set; }
        public decimal WeightCount { get; set; }
        public decimal Price { get; set; }
        
        public WeightType WeightType { get; set; }
        public User User { get; set; }
        public virtual ICollection<MenuInstitution> MenuInstitutions { get; set; }
    }
}
