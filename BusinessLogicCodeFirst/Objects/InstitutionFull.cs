﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Objects
{
    public class InstitutionFull
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Logo { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public List<int> ServiceTag { get; set; }
        public List<int> Kitchen { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string CountryAbbr { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
