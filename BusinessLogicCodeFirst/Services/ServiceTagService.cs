﻿using BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public interface IServiceTagService : IService<ServiceTag>
    {
        
    }

    public class ServiceTagService : BaseService<ServiceTag>, IServiceTagService
    {
        public ServiceTagService(IDataContext dataContext) : base(dataContext) {}

        
    }
}
