﻿using BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Services
{
    public interface IInstitutionTypeService : IService<InstitutionType>
    {
    }

    public class InstitutionTypeService : BaseService<InstitutionType>, IInstitutionTypeService
    {
        public InstitutionTypeService(IDataContext dataContext) : base(dataContext) {}
        
    }
}
