﻿using BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Objects;

namespace BusinessLogic.Services
{
    public interface IInstitutionService : IService<Institution>
    {
        IQueryable<Institution> GetByUserId(int userId);
        void Save(int userId, InstitutionFull info);
    }

    public class InstitutionService : BaseService<Institution>, IInstitutionService
    {
        public InstitutionService(IDataContext dataContext) : base(dataContext) {}

        public IQueryable<Institution> GetByUserId(int userId)
        {
            return dataContext.Users.Where(x => x.Id == userId).FirstOrDefault().Institutions.AsQueryable();
        }

        public void Save(int userId, InstitutionFull info)
        {
            var institution = dataContext.Institutions.Where(x => x.Users.Where(w => w.Id == userId).Count() == 1 && x.Id == info.Id).FirstOrDefault();

            if (institution != null)
            {
                institution.Name = info.Name;
                institution.Country = info.Country;
                institution.Address = info.Address;
                institution.City = info.City;
                institution.Logo = info.Logo;
                institution.InstitutionTypeId = info.TypeId;
                institution.Description = info.Description;
                institution.Logo = info.Logo;
                institution.Lat = info.Lat;
                institution.Lng = info.Lng;
                institution.CountryAbbr = info.CountryAbbr;
                institution.PostalCode = info.PostalCode;

                institution.ServiceTags.Clear();

                var serviceTagList = dataContext.ServiceTags.Where(x => info.ServiceTag.Contains(x.Id)).ToList();
                institution.ServiceTags = serviceTagList;

                institution.Kitchens.Clear();

                var kitchenList = dataContext.Kitchens.Where(x => info.Kitchen.Contains(x.Id)).ToList();
                institution.Kitchens = kitchenList;

                dataContext.SaveChanges();
            }
        }
    }
}
