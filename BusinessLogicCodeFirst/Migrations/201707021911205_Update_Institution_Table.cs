namespace BusinessLogic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Institution_Table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Institution", "CountryAbbr", c => c.String());
            AddColumn("dbo.Institution", "PostalCode", c => c.String());
            AddColumn("dbo.Institution", "Lat", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Institution", "Lng", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Institution", "Lng");
            DropColumn("dbo.Institution", "Lat");
            DropColumn("dbo.Institution", "PostalCode");
            DropColumn("dbo.Institution", "CountryAbbr");
        }
    }
}
