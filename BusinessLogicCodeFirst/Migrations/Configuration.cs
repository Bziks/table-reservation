namespace BusinessLogic.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BusinessLogic.Models.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "BusinessLogic.Models.DataContext";
        }

        protected override void Seed(BusinessLogic.Models.DataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Roles.AddOrUpdate(
                p => p.Name,
                new Role { Name = RoleNames.User },
                new Role { Name = RoleNames.Owner },
                new Role { Name = RoleNames.Admin },
                new Role { Name = RoleNames.Staff }
            );

            context.WeightTypes.AddOrUpdate(
                p => p.Name,
                new WeightType { Name = "��" },
                new WeightType { Name = "������" },
                new WeightType { Name = "�" },
                new WeightType { Name = "��" },
                new WeightType { Name = "�" },
                new WeightType { Name = "��" }
            );

            context.Kitchens.AddOrUpdate(
                p => p.Name,
                new Kitchen { Name = "�������������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "���������������" },
                new Kitchen { Name = "������������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "��������" },
                new Kitchen { Name = "������������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "��������������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "�������������" },
                new Kitchen { Name = "�����������������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "��������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "������������������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "������������" },
                new Kitchen { Name = "������������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "��������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "������������" },
                new Kitchen { Name = "�������������" },
                new Kitchen { Name = "�������" },
                new Kitchen { Name = "������" },
                new Kitchen { Name = "��������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "�����������������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "�������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "��������" },
                new Kitchen { Name = "���������" },
                new Kitchen { Name = "����������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "�������" },
                new Kitchen { Name = "�����������" },
                new Kitchen { Name = "��������" }
            );

            context.ServiceTags.AddOrUpdate(
                p => p.Name,
                new ServiceTag { Name = "Wi-Fi" },
                new ServiceTag { Name = "��������" },
                new ServiceTag { Name = "������� �� �����" },
                new ServiceTag { Name = "���� ��� �������" },
                new ServiceTag { Name = "Cork fee" },
                new ServiceTag { Name = "Dj" },
                new ServiceTag { Name = "VIP ���" },
                new ServiceTag { Name = "���������� ���� �� ���������" },
                new ServiceTag { Name = "������ ����" },
                new ServiceTag { Name = "�������" },
                new ServiceTag { Name = "������� �������" },
                new ServiceTag { Name = "������� ��������" },
                new ServiceTag { Name = "������� ����" },
                new ServiceTag { Name = "����� ������" },
                new ServiceTag { Name = "�������� " },
                new ServiceTag { Name = "�������� ���� ����" },
                new ServiceTag { Name = "��� ��� �������" },
                new ServiceTag { Name = "������" },
                new ServiceTag { Name = "��������������" },
                new ServiceTag { Name = "������ �������" },
                new ServiceTag { Name = "�� ����" },
                new ServiceTag { Name = "�� �����" },
                new ServiceTag { Name = "���������� ���� ���������" },
                new ServiceTag { Name = "���������� ���� �������" },
                new ServiceTag { Name = "������ ��������� ���������" },
                new ServiceTag { Name = "�������� �����" },
                new ServiceTag { Name = "����� �� ������������� �������� � ���" },
                new ServiceTag { Name = "� ���������" },
                new ServiceTag { Name = "�������" },
                new ServiceTag { Name = "���� ��������" }
            );

            context.InstitutionTypes.AddOrUpdate(
                p => p.Name,
                new InstitutionType { Name = "����" },
                new InstitutionType { Name = "����-���" },
                new InstitutionType { Name = "����-���" },
                new InstitutionType { Name = "��������� ���" },
                new InstitutionType { Name = "���" },
                new InstitutionType { Name = "��������" },
                new InstitutionType { Name = "����������-����������� ��������" },
                new InstitutionType { Name = "�������-����" },
                new InstitutionType { Name = "������������" },
                new InstitutionType { Name = "�������" },
                new InstitutionType { Name = "�����" },
                new InstitutionType { Name = "������ ����" },
                new InstitutionType { Name = "���" },
                new InstitutionType { Name = "��������" },
                new InstitutionType { Name = "������� ��������" },
                new InstitutionType { Name = "��������������� ��������" },
                new InstitutionType { Name = "��������" },
                new InstitutionType { Name = "���������" }
            );
        }
    }
}
