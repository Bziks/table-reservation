namespace BusinessLogic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addotherinittable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MenuInstitution",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InstitutionId = c.Int(nullable: false),
                        MenuCategoryId = c.Int(nullable: false),
                        MenuRecipeId = c.Int(nullable: false),
                        Position = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Institution", t => t.InstitutionId, cascadeDelete: false)
                .ForeignKey("dbo.MenuCategory", t => t.MenuCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.MenuRecipe", t => t.MenuRecipeId, cascadeDelete: false)
                .Index(t => t.InstitutionId)
                .Index(t => t.MenuCategoryId)
                .Index(t => t.MenuRecipeId);
            
            CreateTable(
                "dbo.MenuCategory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.MenuRecipe",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        Name = c.String(),
                        Image = c.String(),
                        Description = c.String(unicode: false, storeType: "text"),
                        WeightTypeId = c.Int(nullable: false),
                        WeightCount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.WeightType", t => t.WeightTypeId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.WeightTypeId);
            
            CreateTable(
                "dbo.WeightType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ServiceTag",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ServiceTagInstitution",
                c => new
                    {
                        ServiceTag_Id = c.Int(nullable: false),
                        Institution_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ServiceTag_Id, t.Institution_Id })
                .ForeignKey("dbo.ServiceTag", t => t.ServiceTag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Institution", t => t.Institution_Id, cascadeDelete: true)
                .Index(t => t.ServiceTag_Id)
                .Index(t => t.Institution_Id);
            
            AlterColumn("dbo.Institution", "Description", c => c.String(unicode: false, storeType: "text"));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ServiceTagInstitution", "Institution_Id", "dbo.Institution");
            DropForeignKey("dbo.ServiceTagInstitution", "ServiceTag_Id", "dbo.ServiceTag");
            DropForeignKey("dbo.MenuRecipe", "WeightTypeId", "dbo.WeightType");
            DropForeignKey("dbo.MenuRecipe", "UserId", "dbo.User");
            DropForeignKey("dbo.MenuInstitution", "MenuRecipeId", "dbo.MenuRecipe");
            DropForeignKey("dbo.MenuCategory", "UserId", "dbo.User");
            DropForeignKey("dbo.MenuInstitution", "MenuCategoryId", "dbo.MenuCategory");
            DropForeignKey("dbo.MenuInstitution", "InstitutionId", "dbo.Institution");
            DropIndex("dbo.ServiceTagInstitution", new[] { "Institution_Id" });
            DropIndex("dbo.ServiceTagInstitution", new[] { "ServiceTag_Id" });
            DropIndex("dbo.MenuRecipe", new[] { "WeightTypeId" });
            DropIndex("dbo.MenuRecipe", new[] { "UserId" });
            DropIndex("dbo.MenuCategory", new[] { "UserId" });
            DropIndex("dbo.MenuInstitution", new[] { "MenuRecipeId" });
            DropIndex("dbo.MenuInstitution", new[] { "MenuCategoryId" });
            DropIndex("dbo.MenuInstitution", new[] { "InstitutionId" });
            AlterColumn("dbo.Institution", "Description", c => c.String());
            DropTable("dbo.ServiceTagInstitution");
            DropTable("dbo.ServiceTag");
            DropTable("dbo.WeightType");
            DropTable("dbo.MenuRecipe");
            DropTable("dbo.MenuCategory");
            DropTable("dbo.MenuInstitution");
        }
    }
}
