namespace BusinessLogic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.User", "CompanyId", "dbo.Company");
            DropIndex("dbo.User", new[] { "CompanyId" });
            CreateTable(
                "dbo.Institution",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        Address = c.String(),
                        Logo = c.String(),
                        Description = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        InstitutionTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InstitutionType", t => t.InstitutionTypeId, cascadeDelete: true)
                .Index(t => t.InstitutionTypeId);
            
            CreateTable(
                "dbo.InstitutionType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserInstitution",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Institution_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Institution_Id })
                .ForeignKey("dbo.User", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Institution", t => t.Institution_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Institution_Id);
            
            DropColumn("dbo.User", "CompanyId");
            DropTable("dbo.Company");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.User", "CompanyId", c => c.Int());
            DropForeignKey("dbo.UserInstitution", "Institution_Id", "dbo.Institution");
            DropForeignKey("dbo.UserInstitution", "User_Id", "dbo.User");
            DropForeignKey("dbo.Institution", "InstitutionTypeId", "dbo.InstitutionType");
            DropIndex("dbo.UserInstitution", new[] { "Institution_Id" });
            DropIndex("dbo.UserInstitution", new[] { "User_Id" });
            DropIndex("dbo.Institution", new[] { "InstitutionTypeId" });
            DropTable("dbo.UserInstitution");
            DropTable("dbo.InstitutionType");
            DropTable("dbo.Institution");
            CreateIndex("dbo.User", "CompanyId");
            AddForeignKey("dbo.User", "CompanyId", "dbo.Company", "Id");
        }
    }
}
