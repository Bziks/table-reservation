namespace BusinessLogic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fix_Coordinate_Fields_Institution_Table : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Institution", "Lat", c => c.Double(nullable: false));
            AlterColumn("dbo.Institution", "Lng", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Institution", "Lng", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Institution", "Lat", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
