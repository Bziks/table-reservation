namespace BusinessLogic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fix_Description_Field_Institution_Table : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Institution", "Description", c => c.String(unicode: true, storeType: "ntext"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Institution", "Description", c => c.String(unicode: false));
        }
    }
}
