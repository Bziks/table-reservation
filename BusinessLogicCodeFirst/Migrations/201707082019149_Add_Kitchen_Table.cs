namespace BusinessLogic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Kitchen_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Kitchen",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.KitchenInstitution",
                c => new
                    {
                        Kitchen_Id = c.Int(nullable: false),
                        Institution_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Kitchen_Id, t.Institution_Id })
                .ForeignKey("dbo.Kitchen", t => t.Kitchen_Id, cascadeDelete: true)
                .ForeignKey("dbo.Institution", t => t.Institution_Id, cascadeDelete: true)
                .Index(t => t.Kitchen_Id)
                .Index(t => t.Institution_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KitchenInstitution", "Institution_Id", "dbo.Institution");
            DropForeignKey("dbo.KitchenInstitution", "Kitchen_Id", "dbo.Kitchen");
            DropIndex("dbo.KitchenInstitution", new[] { "Institution_Id" });
            DropIndex("dbo.KitchenInstitution", new[] { "Kitchen_Id" });
            DropTable("dbo.KitchenInstitution");
            DropTable("dbo.Kitchen");
        }
    }
}
