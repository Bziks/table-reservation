﻿var institution = (function (site, moment) {

    function initTileList() {

        var columnActionsTemplate = Handlebars.compile($("#column-actions-template").html());

        $('.institution-list .tile-caffee').each(function (index, tag) {
            var id = $(tag).attr('data-id');
            $(tag).find('.links').html(columnActionsTemplate({ id: id }));
        });
    }

    function initActions() {
        site.modalFormLink('.btn-institution-delete', {
            url: '/Institution/ConfirmDeleteInstitution',
            data: function (el) {
                return { id: el.data('id') };
            },
            formSubmitSuccess: function () {
                console.log('ok');
            }
        });
    }

    function initGoogleMapPlace() {
        var defaultBounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(-33.8902, 151.1759),
            new google.maps.LatLng(-33.8474, 151.2631)
        );

        var input = document.getElementById('search-address');
        var options = {
            bounds: defaultBounds,
            types: ['address']
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();

            $('.institution-map-lat').val(Number(lat));
            $('.institution-map-lng').val(Number(lng));

            for (var i in place.address_components) {
                var address_component = place.address_components[i];
                for (var j in address_component.types) {
                    var address_type = address_component.types[j];
                    if (address_type == "locality") {
                        $('.institution-map-city').val(address_component.long_name);
                    } else if (address_type == "country") {
                        $('.institution-map-country').val(address_component.long_name);
                        $('.institution-map-country-abbr').val(address_component.short_name);
                    } else if (address_type == "postal_code") {
                        $('.institution-map-postal-code').val(address_component.long_name);
                    }
                }
            }
        });
    }

    function initComboBox(serviceTagVal, kitchenVal) {
        $("#service-tags").select2({
            tags: true,
            dropdownAutoWidth: true
        });

        $("#kitchen").select2({
            tags: true,
            dropdownAutoWidth: true
        });

        $("#service-tags").select2().val(serviceTagVal).trigger("change");
        $("#kitchen").select2().val(kitchenVal).trigger("change");
    }

    function initEditor() {
        var editor = new Quill('#editor-container', {
            modules: {
                toolbar: '#toolbar-container'
            },
            placeholder: 'Описание...',
            theme: 'snow'
        });

        try {
            editor.setContents(JSON.parse($(".institution-description").val()));
        } catch(ex) {
            editor.setContents([]);
        }
        

        editor.on('editor-change', function (eventName, ...args) {
            if (eventName === 'text-change') {
                var description = JSON.stringify(editor.getContents());
                $(".institution-description").val(description);
            }
        });
    }

    function initUploader(institutionId) {
        $('#fine-uploader-validation').fineUploader({
            template: 'qq-template-validation',
            request: {
                endpoint: '/institution/uploadphoto',
                params: {
                    id: institutionId
                }
            },
            thumbnails: {
                placeholders: {
                    waitingPath: '/Scripts/plugins/fine-uploader/placeholders/waiting-generic.png',
                    notAvailablePath: '/Scripts/plugins/fine-uploader/placeholders/not_available-generic.png'
                }
            },
            validation: {
                allowedExtensions: ['jpeg', 'jpg', 'png'],
                itemLimit: 3,
                sizeLimit: 5242880 // 5Mb
            },
            callbacks: {
                onComplete: function (id, name, responseJson, xhr) {
                    console.log(arguments);
                }
            }
        });
    }

    function initPhotos() {
        $('#photos').magnificPopup({
            delegate: '.photo-link',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
                verticalFit: true,
                titleSrc: function (item) {
                    return item.el.attr('title');
                }
            },
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function (element) {
                    return element.find('img');
                }
            }

        });
    }

    var me = {};

    me.initMainPage = function () {
        initTileList();
        initActions();
    };

    me.initEditPage = function (institutionId, serviceTagVal, kitchenVal) {
        initComboBox(serviceTagVal, kitchenVal);
        initGoogleMapPlace();
        initEditor();
        initUploader(institutionId);
        initPhotos();
    };

    return me;
})(site, moment);