﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AutoMapper;

namespace WebSite.ViewModels.Institution
{
    using ViewModels.Mapping;
    using BusinessLogic.Models;
    using BusinessLogic.Objects;

    public class Mappings : IViewModelMapping
    {
        public void Create(IMapperConfiguration configuration)
        {
            configuration.CreateMap<Institution, InstitutionListDatatableViewModel>()
                .IgnoreAllNonExisting();

            configuration.CreateMap<Institution, InstitutionShortViewModel>()
                .IgnoreAllNonExisting();

            configuration.CreateMap<Institution, InstitutionFullViewModel>()
                .ForMember(d => d.ServiceTag, o => o.MapFrom(x => x.ServiceTags.Select(s => s.Id)))
                .ForMember(d => d.Kitchen, o => o.MapFrom(x => x.Kitchens.Select(s => s.Id)))
                .ForMember(d => d.TypeId, o => o.MapFrom(x => x.InstitutionTypeId))
                .IgnoreAllNonExisting();

            configuration.CreateMap<ServiceTag, ComboBoxViewModel>()
                .ForMember(d => d.Value, o => o.MapFrom(x => x.Id))
                .IgnoreAllNonExisting();

            configuration.CreateMap<Kitchen, ComboBoxViewModel>()
                .ForMember(d => d.Value, o => o.MapFrom(x => x.Id))
                .IgnoreAllNonExisting();

            configuration.CreateMap<InstitutionType, ComboBoxViewModel>()
                .ForMember(d => d.Value, o => o.MapFrom(x => x.Id))
                .IgnoreAllNonExisting();

            configuration.CreateMap<InstitutionFullViewModel, InstitutionFull>()
                .IgnoreAllNonExisting();
        }
    }
}