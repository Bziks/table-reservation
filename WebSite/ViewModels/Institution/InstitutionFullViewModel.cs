﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebSite.ViewModels.Institution
{
    public class InstitutionFullViewModel
    {
        [Required]
        public int Id { get; set; }
        [Display(Name = "Name", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "NameRequired")]
        public string Name { get; set; }
        [Required]
        public string Country { get; set; }
        [Display(Name = "Address", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "AddressRequired")]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        public string Logo { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "DescriptionRequired")]
        public string Description { get; set; }
        [Display(Name = "InstitutionType", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "InstitutionTypeRequired")]
        public int TypeId { get; set; }
        [Display(Name = "ServiceTag", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "ServiceTagRequired")]
        public List<int> ServiceTag { get; set; }
        [Required]
        public double Lat { get; set; }
        [Required]
        public double Lng { get; set; }
        [Required]
        public string CountryAbbr { get; set; }
        [Required]
        public string PostalCode { get; set; }
        [Display(Name = "Phone", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources), ErrorMessageResourceName = "PhoneRequired")]
        public string Phone { get; set; }
        [Display(Name = "Email", ResourceType = typeof(Resources.Resources))]
        public string Email { get; set; }
        [Display(Name = "WebSite", ResourceType = typeof(Resources.Resources))]
        public string Site { get; set; }
        [Display(Name = "Kitchen", ResourceType = typeof(Resources.Resources))]
        public List<int> Kitchen { get; set; }

        public List<ComboBoxViewModel> InstitutionTypeList { get; set; }
        public List<ComboBoxViewModel> ServiceTagList { get; set; }
        public List<ComboBoxViewModel> KitchenList { get; set; }
        public List<string> PhotoList { get; set; }

        public InstitutionFullViewModel() {
            PhotoList = new List<string>();
        }

        public InstitutionFullViewModel(List<ComboBoxViewModel> serviceTagList, List<ComboBoxViewModel> typeList, List<ComboBoxViewModel> kitchenList)
        {
            PhotoList = new List<string>();
            ServiceTagList = serviceTagList;
            InstitutionTypeList = typeList;
            KitchenList = kitchenList;
        }
    }
}