﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSite.ViewModels
{
    public class ComboBoxViewModel
    {
        public int Value { get; set; }
        public string Name { get; set; }
    }
}