﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;

using DataTables.AspNet.Core;
using DataTables.AspNet.Mvc5;

using AutoMapper;

using DataTables.AspNet.AspNet5.Extensions.Linq;

using BusinessLogic.Services;
using BusinessLogic.Models;

namespace WebSite.Controllers
{
    using BusinessLogic.Objects;
    using Core.Identity;
    using FineUploader;
    using System.IO;
    using ViewModels;
    using ViewModels.Institution;
    using ViewModels.Mapping;

    [Authorize(Roles = "User")]
    public class InstitutionController : BaseController
    {
        private IInstitutionService institutionService;
        private IServiceTagService serviceTagService;
        private IInstitutionTypeService institutionTypeService;
        private IKitchenService kitchenService;

        IMapper mapper;

        private int UserId
        {
            get { return int.Parse(User.Identity.GetUserId()); }
        }

        public InstitutionController(IInstitutionService institutionService, IMapper mapper, IServiceTagService serviceTagService, IInstitutionTypeService institutionTypeService, IKitchenService kitchenService)
        {
            this.institutionService = institutionService;
            this.serviceTagService = serviceTagService;
            this.institutionTypeService = institutionTypeService;
            this.kitchenService = kitchenService;

            this.mapper = mapper;
        }

        // GET: Institution
        public ActionResult Index()
        {
            var institutions = institutionService.GetByUserId(UserId);
            var model = mapper.Map<List<InstitutionShortViewModel>>(institutions);

            return View(model);
        }

        public ActionResult View(int id)
        {
            Institution institution = institutionService.GetByUserId(UserId).Where(x => x.Id == id).FirstOrDefault();

            var model = mapper.Map<InstitutionShortViewModel>(institution);

            return View(model);
        }

        public ActionResult ConfirmDeleteInstitution(int id)
        {
            Institution institution = institutionService.GetByUserId(UserId).Where(x => x.Id == id).FirstOrDefault();

            var model = mapper.Map<InstitutionShortViewModel>(institution);

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new InstitutionShortViewModel();

            return View("Views/Institutions/Edit", model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var serviceTags = serviceTagService.GetAll().ToList();
            List<ComboBoxViewModel> serviceTagList = mapper.Map<List<ServiceTag>, List<ComboBoxViewModel>>(serviceTags);

            var institutionTypes = institutionTypeService.GetAll().ToList();
            List<ComboBoxViewModel> institutionTypeList = mapper.Map<List<InstitutionType>, List<ComboBoxViewModel>>(institutionTypes);

            var kitchens = kitchenService.GetAll().ToList();
            List<ComboBoxViewModel> kitchenList = mapper.Map<List<Kitchen>, List<ComboBoxViewModel>>(kitchens);

            var model = new InstitutionFullViewModel(serviceTagList, institutionTypeList, kitchenList);
            
            Institution institution = institutionService.GetByUserId(UserId).Where(x => x.Id == id).FirstOrDefault();
            mapper.Map<Institution, InstitutionFullViewModel>(institution, model);

            model.ServiceTag = institution.ServiceTags.Select(s => s.Id).ToList();

            string dir = $"{AppDomain.CurrentDomain.BaseDirectory}\\Uploads\\Institutions\\{id}";
            List<string> allowedExtensions = new List<string> { "png", "jpg", "jpeg" };
            
            var files = Directory.GetFiles(dir);
            foreach (var file in files)
            {
                string extension = file.Split('.').LastOrDefault() ?? "none";

                if (allowedExtensions.Contains(extension))
                {
                    FileInfo fileInfo = new FileInfo(file);
                    model.PhotoList.Add($"/uploads/institutions/{id}/{fileInfo.Name}");
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int id, InstitutionFullViewModel model)
        {
            if (ModelState.IsValid)
            {
                InstitutionFull info = mapper.Map<InstitutionFullViewModel, InstitutionFull>(model);

                institutionService.Save(UserId, info);
            }

            var serviceTags = serviceTagService.GetAll().ToList();
            List<ComboBoxViewModel> serviceTagList = mapper.Map<List<ServiceTag>, List<ComboBoxViewModel>>(serviceTags);

            var institutionTypes = institutionTypeService.GetAll().ToList();
            List<ComboBoxViewModel> institutionTypeList = mapper.Map<List<InstitutionType>, List<ComboBoxViewModel>>(institutionTypes);

            var kitchens = kitchenService.GetAll().ToList();
            List<ComboBoxViewModel> kitchenList = mapper.Map<List<Kitchen>, List<ComboBoxViewModel>>(kitchens);

            model.ServiceTagList = serviceTagList;
            model.InstitutionTypeList = institutionTypeList;
            model.KitchenList = kitchenList;

            return View(model);
        }

        [HttpPost]
        public FineUploaderResult UploadPhoto(FineUpload upload, int id)
        {
            FineUploaderResult answer = new FineUploaderResult(false, error: "Error"); ;

            string dir = $"{AppDomain.CurrentDomain.BaseDirectory}\\Uploads\\Institutions\\{id}";
            List<string> allowedExtensions = new List<string> { "png", "jpg", "jpeg" };
            string extension = upload.Filename.Split('.').LastOrDefault() ?? "none";

            if (allowedExtensions.Contains(extension))
            {
                string filename = System.Guid.NewGuid().ToString() + "." + extension;
                string filePath = Path.Combine(dir, filename);

                try
                {
                    upload.SaveAs(filePath);
                }
                catch (Exception ex)
                {
                    answer = new FineUploaderResult(false, error: ex.Message);
                }

                // the anonymous object in the result below will be convert to json and set back to the browser
                answer = new FineUploaderResult(true, new { pictureName = 12345 });
            } else
            {
                answer = new FineUploaderResult(false, error: "It's not image");
            }

            return answer;
        }
    }
}