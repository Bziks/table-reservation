﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;

using DataTables.AspNet.Core;
using DataTables.AspNet.Mvc5;

using AutoMapper;

using DataTables.AspNet.AspNet5.Extensions.Linq;

using BusinessLogic.Services;
using BusinessLogic.Models;

namespace WebSite.Controllers
{
    using Core.Identity;
    using ViewModels.Admin;
    using ViewModels.Mapping;

    [Authorize(Roles="Admin")]
    public class _AdminController : Controller
    {
        private const int PageSize = 10;

        private IdentityUserManager userManager;
        private IdentityRoleManager roleManager;
        private IdentitySignInManager signInManager;

        private IUserService userService;

        IMapper mapper;

        public _AdminController(IdentityUserManager userManager, IdentityRoleManager roleManager, IdentitySignInManager signInManager, IUserService userService, IMapper mapper)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.signInManager = signInManager;

            this.userService = userService;

            this.mapper = mapper;
        }

        public ActionResult UserList()
        {
            var model = new UserListViewModel();
            model.Roles = roleManager.Roles.OrderBy(r => r.Name).ToList().ToSelectListItems(r => r.Name, r => r.Id, true);

            return View(model);
        }

        public ActionResult DisableOrEnableUser(int id)
        {
            var user = userService.GetById(id);

            userService.DisableOrEnable(user);

            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConfirmDeleteUser(int id)
        {
            var user = userService.GetById(id);

            var model = mapper.Map<UserFormViewModel>(user);

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult DeleteUser(int id)
        {
            userService.Delete(userService.GetById(id));

            return Json(new { success = true });
        }

        public ActionResult CreateUser()
        {
            var model = new UserFormViewModel();

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult CreateUser(UserFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newUser = new User();

                newUser.UserName = model.Email;
                newUser.FullName = model.FullName;
                newUser.Email = model.Email;
                newUser.Disabled = model.Disabled;
                newUser.CreateDate = DateTime.Now;

                var result = userManager.CreateAsync(newUser, model.Password).Result;

                if (result.Succeeded)
                {
                    result = userManager.AddToRoleAsync(newUser.Id, RoleNames.User).Result;

                    if (result.Succeeded)
                        return Json(new { success = true });
                }

                CollectIdentityErrors(result);
            }

            var m = new UserFormViewModel();

            return PartialView(m);
        }

        public ActionResult EditUser(int id)
        {
            var user = userService.GetById(id);

            var model = mapper.Map<UserFormViewModel>(user);

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult EditUser(UserFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userToEdit = userManager.FindByIdAsync(model.Id.Value).Result;

                userToEdit.UserName = model.Email;
                userToEdit.FullName = model.FullName;
                userToEdit.Email = model.Email;
                userToEdit.Disabled = model.Disabled;

                var result = userManager.UpdateAsync(userToEdit).Result;

                if (result.Succeeded)
                {
                    if (!String.IsNullOrEmpty(model.ConfirmPassword))
                    {
                        string resetToken = userManager.GeneratePasswordResetTokenAsync(userToEdit.Id).Result;
                        result = userManager.ResetPasswordAsync(userToEdit.Id, resetToken, model.ConfirmPassword).Result;

                        if (result.Succeeded)
                            return Json(new { success = true });
                    }
                    else
                        return Json(new { success = true });
                }

                CollectIdentityErrors(result);
            }

            var m = new UserFormViewModel();

            return PartialView(m);
        }
        
        private void CollectIdentityErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }
    }
}