﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic.Services;

namespace WebSite.Controllers
{
    [Authorize(Roles="User")]
    public class _UserController : Controller
    {
        private IUserService userService;

        public _UserController(IUserService userService)
        {
            this.userService = userService;
        }

        public ActionResult Account()
        {
            return View();
        }
    }
}
