﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Controllers
{
    public class MenuController : BaseController
    {
        // GET: Menu
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Recipe()
        {
            return View();
        }

        public ActionResult Category()
        {
            return View();
        }
    }
}