﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebSite.Controllers
{

    [Authorize(Roles = "User")]
    public class DashboardController : BaseController
    {
        public DashboardController()
        {
            
        }

        //
        // GET: /Manage/Index
        public ActionResult Index()
        {
            return View();
        }
    }
}