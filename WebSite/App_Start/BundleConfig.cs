using System.Web;
using System.Web.Optimization;

namespace WebSite
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/main-scripts").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/tether.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.js",
                        "~/Scripts/jquery.validate.js",
                        "~/Scripts/jquery.validate.unobtrusive.js",
                        "~/Scripts/validate-unobtrusive-bootstrap/jquery.validate.unobtrusive.bootstrap.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/plugins/handlebars/handlebars.js",
                        "~/Scripts/plugins/momentjs/moment.js",
                        "~/Scripts/plugins/debounce/debounce.js",
                        "~/Scripts/plugins/icheck/icheck.min.js",
                        "~/Scripts/plugins/pnotify/pnotify.custom.min.js",
                        "~/Scripts/plugins/daterangepicker/daterangepicker.js",
                        "~/Scripts/plugins/datatables/jquery.dataTables.js",
                        "~/Scripts/plugins/magnific-popup/jquery.magnific-popup.js",
                        "~/Scripts/datatables/datatables-extended.js",
                        "~/Scripts/datatables/datatables-filters.js",
                        "~/Scripts/datatables/datatables-filters-daterangepicker.js",
                        "~/Scripts/datatables/datatables-filters-iCheck.js",
                        "~/Scripts/site/ajax-loader.js",
                        "~/Scripts/site/checkboxes.js",
                        "~/Scripts/site/modals.js",
                        "~/Scripts/site/site.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/admin-user-scripts").Include(
                "~/Scripts/pages/admin/user-list.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/institution-scripts").Include(
                "~/Scripts/plugins/select2/select2.min.js",
                "~/Scripts/plugins/fine-uploader/jquery.fine-uploader.js",
                "~/Scripts/pages/institution/institution.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/institution-styles")
                .Include("~/Content/plugins/select2/select2.min.css", new CssRewriteUrlTransform())
            );

            bundles.Add(new StyleBundle("~/bundles/main-styles")
                .Include("~/Content/flags/flags.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/bootstrap.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/font-awesome.min.css", new CssRewriteUrlTransform())
                .Include("~/Scripts/plugins/fine-uploader/fine-uploader-new.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/magnific-popup/magnific-popup.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/spinkit/spinkit.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/datatables/dataTables.bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/icheck/square/red.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/pnotify/pnotify.custom.min.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/daterangepicker/daterangepicker.css", new CssRewriteUrlTransform())
                .Include("~/Content/style.css", new CssRewriteUrlTransform())
            );

            bundles.Add(new StyleBundle("~/bundles/social-buttons")
                .Include("~/Content/plugins/bootstrap-social/font-awesome.css", new CssRewriteUrlTransform())
                .Include("~/Content/plugins/bootstrap-social/bootstrap-social.css", new CssRewriteUrlTransform())
            );
        }
    }
}